class CMoveData {
public:
	bool            m_bFirstRunOfFunctions : 1;
	bool            m_bGameCodeMovedPlayer : 1;

	int                m_nPlayerHandle;    // edict index on server, client entity handle on client

	int                m_nImpulseCommand;    // Impulse command issued.
	Vector            m_vecViewAngles;    // Command view angles (local space)
	Vector            m_vecAbsViewAngles;    // Command view angles (world space)
	int                m_nButtons;            // Attack buttons.
	int                m_nOldButtons;        // From host_client->oldbuttons;
	float            m_flForwardMove;
	float            m_flSideMove;
	float            m_flUpMove;

	float            m_flMaxSpeed;
	float            m_flClientMaxSpeed;

	// Variables from the player edict (sv_player) or entvars on the client.
	// These are copied in here before calling and copied out after calling.
	Vector            m_vecVelocity;        // edict::velocity        // Current movement direction.
	Vector            m_vecAngles;        // edict::angles
	Vector            m_vecOldAngles;

	// Output only
	float            m_outStepHeight;    // how much you climbed this move
	Vector            m_outWishVel;        // This is where you tried 
	Vector            m_outJumpVel;        // This is your jump velocity

										   // Movement constraints    (radius 0 means no constraint)
	Vector            m_vecConstraintCenter;
	float            m_flConstraintRadius;
	float            m_flConstraintWidth;
	float            m_flConstraintSpeedFactor;

	float            m_flUnknown[5];

	Vector            m_vecAbsOrigin;        // edict::origin
};

class IMoveHelper {
public:
	void SetHost(C_BaseEntity *pPlayer) {
		typedef void(__thiscall* oSetHost)(PVOID, C_BaseEntity*);
		return getvfunc< oSetHost >(this, 1)(this, pPlayer);
	}
};

class IGameMovement {
public:

	void ProcessMovement(C_BaseEntity *pPlayer, void *pMove) {
		typedef void(__thiscall* oProcessMovement)(PVOID, C_BaseEntity*, void*);
		return getvfunc< oProcessMovement >(this, 1)(this, pPlayer, pMove);
	}

	void StartTrackPredictionErrors(C_BaseEntity* player) {
		typedef void(__thiscall* oPredictionError)(PVOID, C_BaseEntity*);
		return getvfunc< oPredictionError >(this, 3)(this, player);
	}

	void FinishTrackPredictionErrors(C_BaseEntity* player) {
		typedef void(__thiscall* oPredictionError)(PVOID, C_BaseEntity*);
		return getvfunc< oPredictionError >(this, 4)(this, player);
	}
};

class IPrediction {
public:

	bool InPrediction() {
		typedef bool(__thiscall* oInPrediction)(PVOID);
		return getvfunc< oInPrediction >(this, 14)(this);
	}

	void RunCommand(C_BaseEntity *player, CUserCmd *ucmd, IMoveHelper *moveHelper) {
		typedef void(__thiscall* oRunCommand)(PVOID, C_BaseEntity*, CUserCmd*, IMoveHelper*);
		return getvfunc< oRunCommand >(this, 19)(this, player, ucmd, moveHelper);
	}

	void SetupMove(C_BaseEntity *player, CUserCmd *ucmd, IMoveHelper *moveHelper, void*pMoveData) {
		typedef void(__thiscall* oSetupMove)(PVOID, C_BaseEntity*, CUserCmd*, IMoveHelper*, void*);
		return getvfunc< oSetupMove >(this, 20)(this, player, ucmd, moveHelper, pMoveData);
	}

	void FinishMove(C_BaseEntity *player, CUserCmd *ucmd, void*pMoveData) {
		typedef void(__thiscall* oFinishMove)(PVOID, C_BaseEntity*, CUserCmd*, void*);
		return getvfunc< oFinishMove >(this, 21)(this, player, ucmd, pMoveData);
	}
};