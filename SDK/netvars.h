typedef struct
{
	char szTableName[256];
	char szPropName[256];
	RecvVarProxyFn SavedProxy;
} Oldproxy_t;

class CNetvars {
public:

	// stores all tables, and all props inside those
	~CNetvars(void);

	// inits all tables, props and offsets.
	void Init(void);

	// calls GetProp wrapper to get the absolute offset of the prop
	int GetOffset(const char *tableName, const char *propName);


	// calls GetProp wrapper to get prop and sets the proxy of the prop
	DWORD HookProp(const char *tableName, const char *propName, RecvVarProxyFn function);


private:

	// wrapper so we can use recursion without too much performance loss
	int GetProp(const char *tableName, const char *propName, RecvProp **prop = 0);


	// uses recursion to return a the relative offset to the given prop and sets the prop param
	int GetProp(RecvTable *recvTable, const char *propName, RecvProp **prop = 0);


	RecvTable *GetTable(const char *tableName);


	std::vector<RecvTable*>    m_tables;

	std::vector<Oldproxy_t> m_savedproxy;
};


extern CNetvars* g_pNetvars;

#define get_netvar(classing, getname, table, prop) \
classing getname() \
{ \
	static int offset = g_pNetvars->GetOffset(table, prop); \
	return *(classing*)((DWORD)this + offset); \
}

#define get_netvar_adder(classing, getname, table, prop, adder) \
classing getname() \
{ \
	static int offset = g_pNetvars->GetOffset(table, prop); \
	return *(classing*)((DWORD)this + (offset + adder)); \
}

#define get_netvar_pointer(classing, getname, table, prop) \
classing* getname() \
{ \
	static int offset = g_pNetvars->GetOffset(table, prop); \
	return (classing*)((DWORD)this + offset); \
}

#define get_netvar_minus(classing, getname, table, prop, adder) \
classing getname() \
{ \
	static int offset = g_pNetvars->GetOffset(table, prop); \
	return *(classing*)((DWORD)this + (offset - adder)); \
}

#define get_static_offset(classing, getname, offset) \
classing getname() \
{ \
	return *(classing*)((DWORD)this + offset); \
}

#define get_static_offset_pointer(classing, getname, offset) \
classing* getname() \
{ \
	return (classing*)((DWORD)this + offset); \
}

#define get_static_offset_double_pointer(classing, getname, offset) \
classing* getname() \
{ \
	return *(classing**)((DWORD)this + offset); \
}

#define get_netvar_single_no_custom(classing, table, prop) \
*reinterpret_cast<classing*>((DWORD)this + g_pNetvars->GetOffset(table, prop));

#define get_netvar_plus_class(classing, getname, adder, table, prop) \
classing getname() \
{ \
	static int offset = g_pNetvars->GetOffset(table, prop); \
	return this->adder() + *(classing*)((DWORD)this + offset); \
}

#define print_netvar(table, prop) \
g_pNetvars->GetOffset(table, prop);

#define make_group(classer, namer, offseter) \
classer namer() { \
	return *reinterpret_cast<classer*>((DWORD)this + offseter); \
}

#define make_pgroup(classer, namer, offseter) \
classer* namer() { \
	return (classer*)((DWORD)this + offseter); \
}

#define make_pgroup_normal(classer, namer, offseter) \
classer* namer() {\
	return *(classer**)((DWORD)this + offseter); \
}