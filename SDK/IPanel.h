class IPanel {
public:
	const char* GetName(VPANEL vguiPanel) {
		typedef const char*(__thiscall* oGetName)(PVOID, VPANEL);
		return getvfunc<oGetName>(this, 36)(this, vguiPanel);
	}
};