class CSWeaponInfo {
public:
	char pad_0x0000[0x850]; //0x0000
	float m_flArmorRatio; //0x0850 
	char pad_0x0854[0x10]; //0x0854
	float m_flPenetration; //0x0864 
	__int32 m_iDamage; //0x0868 
	float m_flRange; //0x086C 
	float m_flRangeModifier; //0x0870 
	char pad_0x0874[0x3CC]; //0x0874
};//Slut size = 0x828.

enum ItemDefinitionIndex
{
	WEAPON_DEAGLE = 1,
	WEAPON_ELITE = 2,
	WEAPON_FIVESEVEN = 3,
	WEAPON_GLOCK = 4,
	WEAPON_AK47 = 7,
	WEAPON_AUG = 8,
	WEAPON_AWP = 9,
	WEAPON_FAMAS = 10,
	WEAPON_G3SG1 = 11,
	WEAPON_GALILAR = 13,
	WEAPON_M249 = 14,
	WEAPON_M4A1 = 16,
	WEAPON_MAC10 = 17,
	WEAPON_P90 = 19,
	WEAPON_UMP45 = 24,
	WEAPON_XM1014 = 25,
	WEAPON_BIZON = 26,
	WEAPON_MAG7 = 27,
	WEAPON_NEGEV = 28,
	WEAPON_SAWEDOFF = 29,
	WEAPON_TEC9 = 30,
	WEAPON_TASER = 31,
	WEAPON_HKP2000 = 32,
	WEAPON_MP7 = 33,
	WEAPON_MP9 = 34,
	WEAPON_NOVA = 35,
	WEAPON_P250 = 36,
	WEAPON_SCAR20 = 38,
	WEAPON_SG553 = 39,
	WEAPON_SSG08 = 40,
	WEAPON_KNIFE = 42,
	WEAPON_FLASHBANG = 43,
	WEAPON_HEGRENADE = 44,
	WEAPON_SMOKEGRENADE = 45,
	WEAPON_MOLOTOV = 46,
	WEAPON_DECOY = 47,
	WEAPON_INCGRENADE = 48,
	WEAPON_C4 = 49,
	WEAPON_KNIFE_T = 59,
	WEAPON_M4A1_SILENCER = 60,
	WEAPON_USP_SILENCER = 61,
	WEAPON_CZ75A = 63,
	WEAPON_REVOLVER = 64,
	WEAPON_KNIFE_BAYONET = 500,
	WEAPON_KNIFE_FLIP = 505,
	WEAPON_KNIFE_GUT = 506,
	WEAPON_KNIFE_KARAMBIT = 507,
	WEAPON_KNIFE_M9_BAYONET = 508,
	WEAPON_KNIFE_TACTICAL = 509,
	WEAPON_KNIFE_FALCHION = 512,
	WEAPON_KNIFE_SURVIVAL_BOWIE = 514,
	WEAPON_KNIFE_BUTTERFLY = 515,
	WEAPON_KNIFE_PUSH = 516,
	GLOVE_STUDDED_BLOODHOUND = 5027,
	GLOVE_T = 5028,
	GLOVE_CT = 5029,
	GLOVE_SPORTY = 5030,
	GLOVE_SLICK = 5031,
	GLOVE_LEATHER_WRAP = 5032,
	GLOVE_MOTORCYCLE = 5033,
	GLOVE_SPECIALIST = 5034,
	GLOVE = GLOVE_T
};

enum EClassIds : int
{
	CAI_BaseNPC = 0,
	CAK47,
	CBaseAnimating,
	CBaseAnimatingOverlay,
	CBaseAttributableItem,
	CBaseButton,
	CBaseCombatCharacter,
	CBaseCombatWeapon,
	CBaseCSGrenade,
	CBaseCSGrenadeProjectile,
	CBaseDoor,
	CBaseEntity,
	CBaseFlex,
	CBaseGrenade,
	CBaseParticleEntity,
	CBasePlayer,
	CBasePropDoor,
	CBaseTeamObjectiveResource,
	CBaseTempEntity,
	CBaseToggle,
	CBaseTrigger,
	CBaseViewModel,
	CBaseVPhysicsTrigger,
	CBaseWeaponWorldModel,
	CBeam,
	CBeamSpotlight,
	CBoneFollower,
	CBreakableProp,
	CBreakableSurface,
	CC4,
	CCascadeLight,
	CChicken,
	CColorCorrection,
	CColorCorrectionVolume,
	CCSGameRulesProxy,
	CCSPlayer,
	CCSPlayerResource,
	CCSRagdoll,
	CCSTeam,
	CDEagle,
	CDecoyGrenade,
	CDecoyProjectile,
	CDynamicLight,
	CDynamicProp,
	CEconEntity,
	CEconWearable,
	CEmbers,
	CEntityDissolve,
	CEntityFlame,
	CEntityFreezing,
	CEntityParticleTrail,
	CEnvAmbientLight,
	CEnvDetailController,
	CEnvDOFController,
	CEnvParticleScript,
	CEnvProjectedTexture,
	CEnvQuadraticBeam,
	CEnvScreenEffect,
	CEnvScreenOverlay,
	CEnvTonemapController,
	CEnvWind,
	CFEPlayerDecal,
	CFireCrackerBlast,
	CFireSmoke,
	CFireTrail,
	CFish,
	CFlashbang,
	CFogController,
	CFootstepControl,
	CFunc_Dust,
	CFunc_LOD,
	CFuncAreaPortalWindow,
	CFuncBrush,
	CFuncConveyor,
	CFuncLadder,
	CFuncMonitor,
	CFuncMoveLinear,
	CFuncOccluder,
	CFuncReflectiveGlass,
	CFuncRotating,
	CFuncSmokeVolume,
	CFuncTrackTrain,
	CGameRulesProxy,
	CHandleTest,
	CHEGrenade,
	CHostage,
	CHostageCarriableProp,
	CIncendiaryGrenade,
	CInferno,
	CInfoLadderDismount,
	CInfoOverlayAccessor,
	CItem_Healthshot,
	CKnife,
	CKnifeGG,
	CLightGlow,
	CMaterialModifyControl,
	CMolotovGrenade,
	CMolotovProjectile,
	CMovieDisplay,
	CParticleFire,
	CParticlePerformanceMonitor,
	CParticleSystem,
	CPhysBox,
	CPhysBoxMultiplayer,
	CPhysicsProp,
	CPhysicsPropMultiplayer,
	CPhysMagnet,
	CPlantedC4,
	CPlasma,
	CPlayerResource,
	CPointCamera,
	CPointCommentaryNode,
	CPoseController,
	CPostProcessController,
	CPrecipitation,
	CPrecipitationBlocker,
	CPredictedViewModel,
	CProp_Hallucination,
	CPropDoorRotating,
	CPropJeep,
	CPropVehicleDriveable,
	CRagdollManager,
	CRagdollProp,
	CRagdollPropAttached,
	CRopeKeyframe,
	CSCAR17,
	CSceneEntity,
	CSensorGrenade,
	CSensorGrenadeProjectile,
	CShadowControl,
	CSlideshowDisplay,
	CSmokeGrenade,
	CSmokeGrenadeProjectile,
	CSmokeStack,
	CSpatialEntity,
	CSpotlightEnd,
	CSprite,
	CSpriteOriented,
	CSpriteTrail,
	CStatueProp,
	CSteamJet,
	CSun,
	CSunlightShadowControl,
	CTeam,
	CTeamplayRoundBasedRulesProxy,
	CTEArmorRicochet,
	CTEBaseBeam,
	CTEBeamEntPoint,
	CTEBeamEnts,
	CTEBeamFollow,
	CTEBeamLaser,
	CTEBeamPoints,
	CTEBeamRing,
	CTEBeamRingPoint,
	CTEBeamSpline,
	CTEBloodSprite,
	CTEBloodStream,
	CTEBreakModel,
	CTEBSPDecal,
	CTEBubbles,
	CTEBubbleTrail,
	CTEClientProjectile,
	CTEDecal,
	CTEDust,
	CTEDynamicLight,
	CTEEffectDispatch,
	CTEEnergySplash,
	CTEExplosion,
	CTEFireBullets,
	CTEFizz,
	CTEFootprintDecal,
	CTEFoundryHelpers,
	CTEGaussExplosion,
	CTEGlowSprite,
	CTEImpact,
	CTEKillPlayerAttachments,
	CTELargeFunnel,
	CTEMetalSparks,
	CTEMuzzleFlash,
	CTEParticleSystem,
	CTEPhysicsProp,
	CTEPlantBomb,
	CTEPlayerAnimEvent,
	CTEPlayerDecal,
	CTEProjectedDecal,
	CTERadioIcon,
	CTEShatterSurface,
	CTEShowLine,
	CTesla,
	CTESmoke,
	CTESparks,
	CTESprite,
	CTESpriteSpray,
	CTest_ProxyToggle_Networkable,
	CTestTraceline,
	CTEWorldDecal,
	CTriggerPlayerMovement,
	CTriggerSoundOperator,
	CVGuiScreen,
	CVoteController,
	CWaterBullet,
	CWaterLODControl,
	CWeaponAug,
	CWeaponAWP,
	CWeaponBaseItem,
	CWeaponBizon,
	CWeaponCSBase,
	CWeaponCSBaseGun,
	CWeaponCycler,
	CWeaponElite,
	CWeaponFamas,
	CWeaponFiveSeven,
	CWeaponG3SG1,
	CWeaponGalil,
	CWeaponGalilAR,
	CWeaponGlock,
	CWeaponHKP2000,
	CWeaponM249,
	CWeaponM3,
	CWeaponM4A1,
	CWeaponMAC10,
	CWeaponMag7,
	CWeaponMP5Navy,
	CWeaponMP7,
	CWeaponMP9,
	CWeaponNegev,
	CWeaponNOVA,
	CWeaponP228,
	CWeaponP250,
	CWeaponP90,
	CWeaponSawedoff,
	CWeaponSCAR20,
	CWeaponScout,
	CWeaponSG550,
	CWeaponSG552,
	CWeaponSG556,
	CWeaponSSG08,
	CWeaponTaser,
	CWeaponTec9,
	CWeaponTMP,
	CWeaponUMP45,
	CWeaponUSP,
	CWeaponXM1014,
	CWorld,
	DustTrail,
	MovieExplosion,
	ParticleSmokeGrenade,
	RocketTrail,
	SmokeTrail,
	SporeExplosion,
	SporeTrail
};


class IClientCombatWeapon {
public:

	make_group(int, Item, g_pOffsets->m_iItemDefinitionIndex);


	bool IsKnife() {
		int ii = this->Item();
		return (ii == 59 || ii == 42 || ii == 500 || ii == 505 || ii == 506 || ii == 507 || ii == 508 || ii == 509 || ii == 512 || ii == 514 || ii == 515 || ii == 516);
	}

	bool IsBomb() {
		return (this->Item() == 49);
	}

	bool IsGrenades() {
		int ii = this->Item();
		return (ii == 43 || ii == 44 || ii == 45 || ii == 46 || ii == 47 || ii == 48);
	}

	bool IsPistol() {
		int ii = this->Item();
		return (ii == 1 || ii == 2 || ii == 3 || ii == 4 || ii == 30 || ii == 31 || ii == 32 || ii == 36 || ii == 61);
	}

	bool IsSniper() {
		int ii = this->Item();
		return (ii == 9 || ii == 8 || ii == 11 || ii == 38 || ii == 39 || ii == 40);
	}

	make_group(int, Clip, g_pOffsets->m_iClip1);
	make_group(float, GetNextPrimaryAttack, g_pOffsets->m_flNextPrimaryAttack);
	make_group(float, FireReadyTime, g_pOffsets->m_flPostponeFireReadyTime);
	make_pgroup(int, GetModelIndex, g_pOffsets->m_iModelIndex);
	make_group(int, GetWearables, g_pOffsets->m_hMyWearables);

	CSWeaponInfo* GetData() {
		typedef CSWeaponInfo*(__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 456)(this);
	}

	float GetInaccuracy() {
		typedef float(__thiscall* OriginalFn)(void*);
		return getvfunc< OriginalFn >(this, 483)(this);
	}

	float GetBestHitchance() {
		float inaccuracy = GetInaccuracy();
		if (inaccuracy == 0)
			inaccuracy = 0.0000001;
		inaccuracy = 1 / inaccuracy;
		return inaccuracy;
	}

	void UpdateAccuracyPenalty() {
		typedef void(__thiscall* oUpdateAccuracyPenalty)(PVOID);
		return getvfunc<oUpdateAccuracyPenalty>(this, 485)(this);
	}

	const char* GetName() {
		typedef const char* (__thiscall* tGetName)(PVOID);
		return getvfunc<tGetName>(this, 378)(this);
	}

	const char* GetPrintName() {
		typedef const char* (__thiscall* tGetName)(PVOID);
		return getvfunc<tGetName>(this, 379)(this);
	}

	float GetSpread() {
		typedef float(__thiscall* OriginalFn)(void*);
		return getvfunc< OriginalFn >(this, 484)(this);
	}

	void SetModelIndex(int modelIndex) {
		typedef void(__thiscall* OriginalFn)(PVOID, int);
		return getvfunc<OriginalFn>(this, 75)(this, modelIndex);
	}

	void PreDataUpdate(int updateType) {
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef void(__thiscall* OriginalFn)(PVOID, int);
		return getvfunc<OriginalFn>(pNetworkable, 6)(pNetworkable, updateType);
	}

	char* GetWeaponStrings() {
		if (!this)
			return "";

		int id = this->Item();


		switch (id) {
		case WEAPON_DEAGLE:
			return "deagle";
		case WEAPON_ELITE:
			return "elites";
		case WEAPON_FIVESEVEN:
			return "five-seven";
		case WEAPON_GLOCK:
			return "glock";
		case WEAPON_AK47:
			return "ak47";
		case WEAPON_AUG:
			return "aug";
		case WEAPON_AWP:
			return "awp";
		case WEAPON_FAMAS:
			return "famas";
		case WEAPON_G3SG1:
			return "g3sg1";
		case WEAPON_GALILAR:
			return "galil";
		case WEAPON_M249:
			return "m249";
		case WEAPON_M4A1:
			return "m4a4";
		case WEAPON_MAC10:
			return "mac10";
		case WEAPON_P90:
			return "p90";
		case WEAPON_UMP45:
			return "ump45";
		case WEAPON_XM1014:
			return "xm1014";
		case WEAPON_BIZON:
			return "bizon";
		case WEAPON_MAG7:
			return "mag7";
		case WEAPON_NEGEV:
			return "negev";
		case WEAPON_SAWEDOFF:
			return "sawed off";
		case WEAPON_TEC9:
			return "tec9";
		case WEAPON_TASER:
			return "zeus";
		case WEAPON_HKP2000:
			return "p2000";
		case WEAPON_MP7:
			return "mp7";
		case WEAPON_MP9:
			return "mp9";
		case WEAPON_NOVA:
			return "nova";
		case WEAPON_P250:
			return "p250";
		case WEAPON_SCAR20:
			return "scar20";
		case WEAPON_SG553:
			return "sg553";
		case WEAPON_SSG08:
			return "ssg08";
		case WEAPON_KNIFE:
			return "knife";
		case WEAPON_FLASHBANG:
			return "flash";
		case WEAPON_HEGRENADE:
			return "hegrenade";
		case WEAPON_SMOKEGRENADE:
			return "smoke";
		case WEAPON_MOLOTOV:
			return "molotov";
		case WEAPON_DECOY:
			return "decoy";
		case WEAPON_INCGRENADE:
			return "inc grenade";
		case WEAPON_C4:
			return "c4";
		case WEAPON_KNIFE_T:
			return "knife";
		case WEAPON_M4A1_SILENCER:
			return "m4a1-s";
		case WEAPON_USP_SILENCER:
			return "usp-s";
		case WEAPON_CZ75A:
			return "cz75";
		case WEAPON_REVOLVER:
			return "revolver";
		case WEAPON_KNIFE_KARAMBIT:
			return "knife";
		case WEAPON_KNIFE_M9_BAYONET:
			return "knife";
		case WEAPON_KNIFE_TACTICAL:
			return "knife";
		case WEAPON_KNIFE_FALCHION:
			return "knife";
		case WEAPON_KNIFE_SURVIVAL_BOWIE:
			return "knife";
		case WEAPON_KNIFE_BUTTERFLY:
			return "knife";
		case WEAPON_KNIFE_PUSH:
			return "knife";

		default:
			return "knife";
		}

		return "";
	}
};