#define Assert( _exp ) ((void)0)


struct mstudiobbox_t
{
	int bone;
	int group; // intersection group
	Vector bbmin; // bounding box 
	Vector bbmax;
	int hitboxnameindex; // offset to the name of the hitbox.
	int pad[3];
	float radius;
	int pad2[4];

	const char* pszHitboxName() const
	{
		if (hitboxnameindex == 0)
			return "";

		return ((const char*)this) + hitboxnameindex;
	}
};
/************************************************************************/
/* Structure: mstudiohitboxset_t                                        */
/************************************************************************/
struct mstudiohitboxset_t
{
	int	sznameindex;

	inline char* const pszName(void) const
	{
		return ((char*)this) + sznameindex;
	}

	int numhitboxes;
	int hitboxindex;

	inline mstudiobbox_t* GetHitbox(int i) const
	{
		return (mstudiobbox_t*)(((BYTE*)this) + hitboxindex) + i;
	};
};
/************************************************************************/
/* Structure: studiohdr_t                                               */
/************************************************************************/

struct mstudiobone_t {
	int                                     sznameindex;
	inline char * const GetName(void) const { return ((char *)this) + sznameindex; }
	int                                     parent;
	int                                     bonecontroller[6];

	Vector                          pos;
	float                           quat[4];
	Vector                          rot;
	Vector                          posscale;
	Vector                          rotscale;

	matrix3x4                     poseToBone;
	float                           qAlignment[4];
	int                                     flags;
	int                                     proctype;
	int                                     procindex;              // procedural rule
	mutable int                     physicsbone;    // index into physically simulated bone
	inline void *           GetProcedure() const { if (procindex == 0) return NULL; else return  (void *)(((byte *)this) + procindex); };
	int                                     surfacepropidx; // index into string tablefor property name
	inline char * const GetSurfaceProps(void) const { return ((char *)this) + surfacepropidx; }
	int                                     contents;               // See BSPFlags.h for the contents flags

	int                                     unused[8];              // remove as appropriate
};

struct studiohdr_t {
	int					id;
	int					version;

	int					checksum;		// this has to be the same in the phy and vtx files to load!

	char				name[64];
	int					length;


	Vector				eyeposition;	// ideal eye position

	Vector				illumposition;	// illumination center

	Vector				hull_min;		// ideal movement hull size
	Vector				hull_max;

	Vector				view_bbmin;		// clipping bounding box
	Vector				view_bbmax;

	int					flags;

	int					numbones;			// bones
	int					boneindex;

	inline mstudiobone_t *GetBone(int i) const { return (mstudiobone_t *)(((byte *)this) + boneindex) + i; };
	//inline mstudiobone_t *pBone(int i) const { Assert(i >= 0 && i < numbones); return (mstudiobone_t *)(((byte *)this) + boneindex) + i; };
	mstudiohitboxset_t* GetHitboxSet(int i) const {
		return (mstudiohitboxset_t*)(((byte*)this) + hitboxsetindex) + i;
	}

	inline mstudiobbox_t* GetHitbox(int i, int set) const {
		mstudiohitboxset_t const* s = GetHitboxSet(set);

		if (!s)
			return NULL;

		return s->GetHitbox(i);
	}

	inline int GetHitboxCount(int set) const {
		mstudiohitboxset_t const* s = GetHitboxSet(set);

		if (!s)
			return 0;

		return s->numhitboxes;
	}

	int					RemapSeqBone(int iSequence, int iLocalBone) const;	// maps local sequence bone to global bone
	int					RemapAnimBone(int iAnim, int iLocalBone) const;		// maps local animations bone to global bone
	inline mstudiobone_t *pBone(int i) const { Assert(i >= 0 && i < numbones); return (mstudiobone_t *)(((byte *)this) + boneindex) + i; };
	int					numbonecontrollers;		// bone controllers
	int					bonecontrollerindex;

	int					numhitboxsets;
	int					hitboxsetindex;

	// Look up hitbox set by index
	mstudiohitboxset_t	*pHitboxSet(int i) const {
		Assert(i >= 0 && i < numhitboxsets);
		return (mstudiohitboxset_t *)(((BYTE *)this) + hitboxsetindex) + i;
	};

	// Calls through to hitbox to determine size of specified set
	inline mstudiobbox_t *pHitbox(int i, int set) const {
		mstudiohitboxset_t const *s = pHitboxSet(set);
		if (!s)
			return NULL;

		return s->GetHitbox(i);
	};

	// Calls through to set to get hitbox count for set
	inline int			iHitboxCount(int set) const {
		mstudiohitboxset_t const *s = pHitboxSet(set);
		if (!s)
			return 0;

		return s->numhitboxes;
	};

	// file local animations? and sequences
	//private:
	int					numlocalanim;			// animations/poses
	int					localanimindex;		// animation descriptions

	int					numlocalseq;				// sequences
	int					localseqindex;

	//public:
	bool				SequencesAvailable() const;
	int					GetNumSeq() const;
	int					iRelativeAnim(int baseseq, int relanim) const;	// maps seq local anim reference to global anim index
	int					iRelativeSeq(int baseseq, int relseq) const;		// maps seq local seq reference to global seq index

																			//private:
	mutable int			activitylistversion;	// initialization flag - have the sequences been indexed?
	mutable int			eventsindexed;
	//public:
	int					GetSequenceActivity(int iSequence);
	void				SetSequenceActivity(int iSequence, int iActivity);
	int					GetActivityListVersion(void);
	void				SetActivityListVersion(int version) const;
	int					GetEventListVersion(void);
	void				SetEventListVersion(int version);

	// raw textures
	int					numtextures;
	int					textureindex;


	// raw textures search paths
	int					numcdtextures;
	int					cdtextureindex;
	inline char			*pCdtexture(int i) const { return (((char *)this) + *((int *)(((BYTE *)this) + cdtextureindex) + i)); };

	// replaceable textures tables
	int					numskinref;
	int					numskinfamilies;
	int					skinindex;
	inline short		*pSkinref(int i) const { return (short *)(((BYTE *)this) + skinindex) + i; };

	int					numbodyparts;
	int					bodypartindex;

	// queryable attachable points
	//private:
	int					numlocalattachments;
	int					localattachmentindex;
	//public:
	int					GetNumAttachments(void) const;
	int					GetAttachmentBone(int i);
	// used on my tools in hlmv, not persistant
	void				SetAttachmentBone(int iAttachment, int iBone);

	// animation node to animation node transition graph
	//private:
	int					numlocalnodes;
	int					localnodeindex;
	int					localnodenameindex;
	inline char			*pszLocalNodeName(int iNode) const { Assert(iNode >= 0 && iNode < numlocalnodes); return (((char *)this) + *((int *)(((BYTE *)this) + localnodenameindex) + iNode)); }
	inline BYTE			*pLocalTransition(int i) const { Assert(i >= 0 && i < (numlocalnodes * numlocalnodes)); return (BYTE *)(((BYTE *)this) + localnodeindex) + i; };

	//public:
	int					EntryNode(int iSequence);
	int					ExitNode(int iSequence);
	char				*pszNodeName(int iNode);
	int					GetTransition(int iFrom, int iTo) const;

	int					numflexdesc;
	int					flexdescindex;

	int					numflexcontrollers;
	int					flexcontrollerindex;

	int					numflexrules;
	int					flexruleindex;

	int					numikchains;
	int					ikchainindex;

	int					nummouths;
	int					mouthindex;

	//private:
	int					numlocalposeparameters;
	int					localposeparamindex;
	//public:
	int					GetNumPoseParameters(void) const;
	int					GetSharedPoseParameter(int iSequence, int iLocalPose) const;

	int					surfacepropindex;
	inline char * const pszSurfaceProp(void) const { return ((char *)this) + surfacepropindex; }

	// Key values
	int					keyvalueindex;
	int					keyvaluesize;
	inline const char * KeyValueText(void) const { return keyvaluesize != 0 ? ((char *)this) + keyvalueindex : NULL; }

	int					numlocalikautoplaylocks;
	int					localikautoplaylockindex;

	int					GetNumIKAutoplayLocks(void) const;
	int					CountAutoplaySequences() const;
	int					CopyAutoplaySequences(unsigned short *pOut, int outCount) const;
	int					GetAutoplayList(unsigned short **pOut) const;

	// The collision model mass that jay wanted
	float				mass;
	int					contents;

	// external animations, models, etc.
	int					numincludemodels;
	int					includemodelindex;
	// implementation specific call to get a named model
	const studiohdr_t	*FindModel(void **cache, char const *modelname) const;

	// implementation specific back pointer to virtual data
	mutable void		*virtualModel;
	//virtualmodel_t		GetVirtualModel(void) const;

	// for demand loaded animation blocks
	int					szanimblocknameindex;
	inline char * const pszAnimBlockName(void) const { return ((char *)this) + szanimblocknameindex; }
	int					numanimblocks;
	int					animblockindex;
	mutable void		*animblockModel;
	BYTE *				GetAnimBlock(int i) const;

	int					bonetablebynameindex;
	inline const BYTE	*GetBoneTableSortedByName() const { return (BYTE *)this + bonetablebynameindex; }

	// used by tools only that don't cache, but persist mdl's peer data
	// engine uses virtualModel to back link to cache pointers
	void				*pVertexBase;
	void				*pIndexBase;

	// if STUDIOHDR_FLAGS_CONSTANT_DIRECTIONAL_LIGHT_DOT is set,
	// this value is used to calculate directional components of lighting 
	// on static props
	BYTE				constdirectionallightdot;

	// set during load of mdl data to track *desired* lod configuration (not actual)
	// the *actual* clamped root lod is found in studiohwdata
	// this is stored here as a global store to ensure the staged loading matches the rendering
	BYTE				rootLOD;

	// set in the mdl data to specify that lod configuration should only allow first numAllowRootLODs
	// to be set as root LOD:
	//	numAllowedRootLODs = 0	means no restriction, any lod can be set as root lod.
	//	numAllowedRootLODs = N	means that lod0 - lod(N-1) can be set as root lod, but not lodN or lower.
	BYTE				numAllowedRootLODs;

	BYTE				unused[1];

	int					unused4; // zero out if version < 47

	int					numflexcontrollerui;
	int					flexcontrolleruiindex;
	int					unused3[2];

	// FIXME: Remove when we up the model version. Move all fields of studiohdr2_t into studiohdr_t.
	int					studiohdr2index;


	// NOTE: No room to add stuff? Up the .mdl file format version 
	// [and move all fields in studiohdr2_t into studiohdr_t and kill studiohdr2_t],
	// or add your stuff to studiohdr2_t. See NumSrcBoneTransforms/SrcBoneTransform for the pattern to use.
	int					unused2[1];

	studiohdr_t() {}

private:
	// No copy constructors allowed
	studiohdr_t(const studiohdr_t& vOther);

	friend struct virtualmodel_t;
};

typedef unsigned short MaterialHandle_t;

class materialsystem_padder {
public:
	virtual void Func0();
	virtual void Func1();
	virtual void Func2();
	virtual void Func3();
	virtual void Func4();
	virtual void Func5();
	virtual void Func6();
	virtual void Func7();
	virtual void Func8();
	virtual void Func9();
	virtual void Func10();
	virtual void Func11();
	virtual void Func12();
	virtual void Func13();
	virtual void Func14();
	virtual void Func15();
	virtual void Func16();
	virtual void Func17();
	virtual void Func18();
	virtual void Func19();
	virtual void Func20();
	virtual void Func21();
	virtual void Func22();
	virtual void Func23();
	virtual void Func24();
	virtual void Func25();
	virtual void Func26();
	virtual void Func27();
	virtual void Func28();
	virtual void Func29();
	virtual void Func30();
	virtual void Func31();
	virtual void Func32();
	virtual void Func33();
	virtual void Func34();
	virtual void Func35();
	virtual void Func36();
	virtual void Func37();
	virtual void Func38();
	virtual void Func39();
	virtual void Func40();
	virtual void Func41();
	virtual void Func42();
	virtual void Func43();
	virtual void Func44();
	virtual void Func45();
	virtual void Func46();
	virtual void Func47();
	virtual void Func48();
	virtual void Func49();
	virtual void Func50();
	virtual void Func51();
	virtual void Func52();
	virtual void Func53();
	virtual void Func54();
	virtual void Func55();
	virtual void Func56();
	virtual void Func57();
	virtual void Func58();
	virtual void Func59();
	virtual void Func60();
	virtual void Func61();
	virtual void Func62();
	virtual void Func63();
	virtual void Func64();
	virtual void Func65();
	virtual void Func66();
	virtual void Func67();
	virtual void Func68();
	virtual void Func69();
	virtual void Func70();
	virtual void Func71();
	virtual void Func72();
	virtual void Func73();
	virtual void Func74();
	virtual void Func75();
	virtual void Func76();
	virtual void Func77();
	virtual void Func78();
	virtual void Func79();
	virtual void Func80();
	virtual void Func81();
	virtual void Func82();
};

class IVMaterialSystem : public materialsystem_padder {
public:
	virtual IMaterial* CreateMaterial(const char* pMaterialName, PVOID pVMTKeyValues);
	virtual IMaterial* FindMaterial(const char* pMaterialName, const char* pTextureGroupName, bool complain = true, const char* pComplainPrefix = NULL);
	virtual void Func85();
	virtual MaterialHandle_t FirstMaterial();
	virtual MaterialHandle_t NextMaterial(MaterialHandle_t h);
	virtual MaterialHandle_t InvalidMaterial();
	virtual IMaterial* GetMaterial(MaterialHandle_t h);
};

class modelrender_padder {
public:
	virtual void Func0();
};

enum EType {
	OVERRIDE_NORMAL = 0,
	OVERRIDE_BUILD_SHADOWS = 1,
	OVERRIDE_DEPTH_WRITE = 2,
	OVERRIDE_WHATEVER = 3
};

typedef float matrix3x4[3][4];

struct ModelRender_t {
	Vector origin;
	Vector angles;
	C_BaseEntity *pRenderable;
	const model_t *pModel;
	const matrix3x4 *pModelToWorld;
	const matrix3x4 *pLightingOffset;
	const Vector *pLightingOrigin;
	int flags;
	int entity_index;
	int skin;
	int body;
	int hitboxset;
	unsigned short instance;
	ModelRender_t() {
		pModelToWorld = NULL;
		pLightingOffset = NULL;
		pLightingOrigin = NULL;
	}
};

class IVModelRender : public modelrender_padder{
public:

	virtual void ForcedMaterialOverride(IMaterial *newMaterial, EType nOverrideType = OVERRIDE_NORMAL, int unknown = NULL);
	virtual bool IsForcedMaterialOverride();
};

class renderview_padder {
public:
	virtual void Func0();
	virtual void Func1();
	virtual void Func2();
	virtual void Func3();
};

class IVRenderView : public renderview_padder {
public:
	virtual void SetBlend(float blend);
	virtual float GetBlend();
	virtual void SetColorModulation(float  const* blend);
	virtual void GetColorModulation(float* blend);
};

class IMaterial {
public:
	virtual const char* GetName();
	virtual const char* GetTextureGroupName();
	virtual void Func2();
	virtual void Func3();
	virtual void Func4();
	virtual void Func5();
	virtual void Func6();
	virtual void Func7();
	virtual void Func8();
	virtual void Func9();
	virtual void Func10();
	virtual void Func11();
	virtual void IncrementReferenceCount();
	virtual void Func13();
	virtual void Func14();
	virtual void Func15();
	virtual void Func16();
	virtual void Func17();
	virtual void Func18();
	virtual void Func19();
	virtual void Func20();
	virtual void Func21();
	virtual void Func22();
	virtual void Func23();
	virtual void Func24();
	virtual void Func25();
	virtual void Func26();
	virtual void AlphaModulate(float alpha);
	virtual void ColorModulate(float r, float g, float b);
	virtual void SetVarFlag(EFlags flag, bool on);
};


class CModelInfo {
public:
	void* GetModel(int Index) 
	{
		typedef void*(__thiscall* OriginalFn)(PVOID, int);
		return getvfunc<OriginalFn>(this, 1)(this, Index);
	}
	int GetModelIndex(const char* ModelName)
	{
		typedef int(__thiscall* OriginalFn)(PVOID, const char*);
		return getvfunc<OriginalFn>(this, 2)(this, ModelName);
	}
	const char* GetModelName(const model_t *model)
	{
		typedef const char* (__thiscall* OriginalFn)(PVOID, const model_t*);
		return getvfunc<OriginalFn>(this, 3)(this, model);
	}
	studiohdr_t     *GetStudiomodel(const model_t *mod)
	{
		typedef studiohdr_t* (__thiscall* OriginalFn)(PVOID, const model_t*);
		return getvfunc<OriginalFn>(this, 30)(this, mod);
	}
	void GetModelMaterials(const model_t *model, int count, IMaterial** ppMaterial)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, const model_t*, int, IMaterial**);
		getvfunc<OriginalFn>(this, 17)(this, model, count, ppMaterial);
	}
	void SetVarFlags(EFlags flag, bool on)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, EFlags, bool);
		getvfunc<OriginalFn>(this, 29)(this, flag, on);
	}
	void SetColorModulation(float const* blend)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, float const*);
		return getvfunc<OriginalFn>(this, 6)(this, blend);
	}
};