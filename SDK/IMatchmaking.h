
class CMatchEventsSubscription;
class CMatchSessionOnlineHost;
class KeyValues_k;

class CMatchFramework
{
public:
	CMatchEventsSubscription* GetEventsSubscription()
	{
		typedef CMatchEventsSubscription*(__thiscall* tGetEventsSubscription)(void*);
		return getvfunc<tGetEventsSubscription>(this, 11)(this);
	}

	CMatchSessionOnlineHost* GetMatchSession()
	{
		typedef CMatchSessionOnlineHost*(__thiscall* tGetMatchSession)(void*);
		return getvfunc<tGetMatchSession>(this, 13)(this);
	}

	CMatchSystem* GetMatchSystem()
	{
		typedef CMatchSystem*(__thiscall* tGetMatchSystem)(void*);
		return getvfunc<tGetMatchSystem>(this, 15)(this);
	}

};

class CMatchSessionOnlineHost
{
public:
	KeyValues* GetSessionSettings()
	{
		typedef KeyValues*(__thiscall* tGetSessionSettings)(void*);
		return getvfunc<tGetSessionSettings>(this, 1)(this);
	}
	void UpdateSessionSettings(KeyValues* packet)
	{
		typedef void(__thiscall* tUpdateSessionSettings)(void*, KeyValues*);
		getvfunc<tUpdateSessionSettings>(this, 2)(this, packet);
	}

	void Command(KeyValues_k* cmd)
	{
		typedef void(__thiscall* OriginalFn)(void*, KeyValues_k*);
		return getvfunc<OriginalFn>(this, 3)(this, cmd);
	}
};