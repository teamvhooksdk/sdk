struct Ray_t;
class CGameTrace;
class ICollideable {
public:
	virtual void GetEntityHandle(void) = 0;
	virtual const Vector& OBBMins(void)const = 0;
	virtual const Vector& OBBMaxs(void)const = 0;
	virtual void WorldSpaceTriggerBounds(Vector *, Vector *)const = 0;
	virtual bool TestCollision(Ray_t const&, unsigned int, CGameTrace &) = 0;
	virtual bool TestHitboxes(Ray_t const&, unsigned int, CGameTrace &) = 0;
	virtual void GetCollisionModelIndex(void) = 0;
	virtual void GetCollisionModel(void) = 0;
	virtual void GetCollisionOrigin(void)const = 0;
	virtual void GetCollisionAngles(void)const = 0;
	virtual void CollisionToWorldTransform(void)const = 0;
	virtual int GetSolid(void)const = 0;
	virtual void GetSolidFlags(void)const = 0;
	virtual void GetIClientUnknown(void) = 0;
	virtual void GetCollisionGroup(void)const = 0;
	virtual void WorldSpaceSurroundingBounds(Vector *, Vector *) = 0;
	virtual void GetRequiredTriggerFlags(void)const = 0;
	virtual void GetRootParentToWorldTransform(void)const = 0;
	virtual void GetVPhysicsObject(void)const = 0;
	virtual void NetworkStateChanged(void) = 0;
	virtual void NetworkStateChanged(void *) = 0;
	virtual void GetPredDescMap(void) = 0;
};