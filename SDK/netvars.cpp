#include "main.h"


void CNetvars::Init(void) {
	m_tables.clear();
	m_savedproxy.clear();

	ClientClass *clientClass = client->GetAllClasses();

	if (!clientClass) {
		return;
	}

	while (clientClass) {
		RecvTable *recvTable = clientClass->GetTable();

		m_tables.push_back(recvTable);

		clientClass = clientClass->NextClass();
	}
}

CNetvars::~CNetvars(void) {
	for (unsigned int i = 0; i < m_savedproxy.size(); i++) {
		RecvProp *recvProp = 0;
		GetProp(m_savedproxy[i].szTableName, m_savedproxy[i].szPropName, &recvProp);

		if (!recvProp)
			return;

		recvProp->m_ProxyFn = m_savedproxy[i].SavedProxy;
	}
}

// calls GetProp wrapper to get the absolute offset of the prop
int CNetvars::GetOffset(const char *tableName, const char *propName) {
	int offset = GetProp(tableName, propName);

	if (!offset) {
		cvar->ConsoleColorPrintf(Color::Red(), "Error finding %s from %s\n", propName, tableName);
		return 0;
	}
	else {
		cvar->ConsoleColorPrintf(Color::Green(), "[+] %s -> 0x%X\n", propName, offset);
	}

	return offset;
}


// calls GetProp wrapper to get prop and sets the proxy of the prop
DWORD CNetvars::HookProp(const char *tableName, const char *propName, RecvVarProxyFn function) {
	RecvProp* recvProp = 0;
	GetProp(tableName, propName, &recvProp);

	if (!recvProp)
		return 0;

	uintptr_t old = (uintptr_t)recvProp->m_ProxyFn;
	recvProp->m_ProxyFn = function;
	return old;
}


// wrapper so we can use recursion without too much performance loss
int CNetvars::GetProp(const char *tableName, const char *propName, RecvProp **prop) {
	RecvTable *recvTable = GetTable(tableName);

	if (!recvTable) {
		printf("[ERROR] Failed to find table: %s\n", tableName);
		return 0;
	}


	int offset = GetProp(recvTable, propName, prop);

	if (!offset) {
		printf("[ERROR] Failed to find prop: %s from table: %s\n", propName, tableName);
		return 0;
	}
	else {
		printf("[+] Found %s -> 0x%X\n", propName, offset);
	}


	return offset;
}


// uses recursion to return a the relative offset to the given prop and sets the prop param
int CNetvars::GetProp(RecvTable *recvTable, const char *propName, RecvProp **prop) {
	int extraOffset = 0;

	for (int i = 0; i < recvTable->m_nProps; ++i) {
		RecvProp *recvProp = &recvTable->m_pProps[i];


		RecvTable *child = recvProp->m_pDataTable;

		if (child && (child->m_nProps > 0)) {
			int tmp = GetProp(child, propName, prop);

			if (tmp) {
				extraOffset += (recvProp->m_Offset + tmp);
			}
		}


		if (_stricmp(recvProp->m_pVarName, propName)) {
			continue;
		}


		if (prop) {
			*prop = recvProp;
		}

		return (recvProp->m_Offset + extraOffset);
	}

	return extraOffset;
}


RecvTable *CNetvars::GetTable(const char *tableName) {
	if (m_tables.empty()) {
		printf("Failed to find table: %s (m_tables is empty)", tableName);

		return 0;
	}


	for each (RecvTable *table in m_tables) {
		if (!table) {
			continue;
		}


		if (_stricmp(table->m_pNetTableName, tableName) == 0) {
			return table;
		}
	}

	return 0;
}

CNetvars* g_pNetvars = new CNetvars();