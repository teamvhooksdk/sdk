class C_BaseAnimating;
class PlayerLocal;
class PlayerManager;
class CMatchSystem;

class C_BaseEntity {
public:

	/* Non Statics. */
	make_group(int, Health, g_pOffsets->m_iHealth);
	make_group(int, Team, g_pOffsets->m_iTeamNum);
	make_group(int, Flags, g_pOffsets->m_fFlags);
	make_group(int, GetTickBase, g_pOffsets->m_nTickBase);
	make_group(int, GetArmour, g_pOffsets->m_ArmorValue);
	make_group(int, GetHelmet, g_pOffsets->m_bHasHelmet);
	make_group(float, GetLowerBodyYaw, g_pOffsets->m_flLowerBodyYawTarget);
	make_group(Vector, GetVelocity, g_pOffsets->m_vecVelocity);
	make_group(bool, GetScoped, g_pOffsets->m_bIsScoped);
	make_group(Vector, GetPunch, g_pOffsets->m_aimPunchAngle);
	make_group(char, GetLifeState, g_pOffsets->m_lifeState);
	make_group(bool, Immune, g_pOffsets->m_bGunGameImmunity);
	make_group(Vector, GetAbsOrigin, g_pOffsets->m_vecOrigin);
	make_group(Vector, GetEyeAngles_NonPointer, g_pOffsets->m_angEyeAngles);
	make_group(int, GetOwner, g_pOffsets->m_hOwnerEntity);
	make_group(int, GetHitboxSet, g_pOffsets->m_nHitboxSet);
	make_group(float, GetCycle, g_pOffsets->m_flCycle);
	make_group(int, GetSimulationTime, g_pOffsets->m_flSimulationTime);
	make_group(int, GetSequence, g_pOffsets->m_nSequence);
	make_group(MoveType_t, GetMoveType, g_pOffsets->m_MoveType);
//	make_pgroup(ICollideable, CollisionGroup, g_pOffsets->m_Collision);
	make_pgroup(Vector, GetEyeAngles, g_pOffsets->m_angEyeAngles);
	make_pgroup_normal(model_t, GetModel, g_pOffsets->m_Model);
	
	Vector GetEyePos() {
		return this->GetAbsOrigin() + *reinterpret_cast<Vector*>((DWORD)this + g_pOffsets->m_vecViewOffset);
	}

	Vector& GetAbsAngles()
	{
		typedef Vector&(__thiscall* oGetAbsAngles)(PVOID);
		return getvfunc<oGetAbsAngles>(this, 11)(this);
	}

	int GetIndex() {
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef int(__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(pNetworkable, 10)(pNetworkable);
	}

	bool SetupBones(matrix3x4 *pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime) {
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef bool(__thiscall* oSetup)(PVOID, matrix3x4*, int, int, int);
		return getvfunc<oSetup>(pRenderable, 13)(pRenderable, pBoneToWorldOut, nMaxBones, boneMask, currentTime);
	}

	bool IsValid() {
		return (GetLifeState() == 0 && !this->Dormant() && this->Health() > 0 && !this->Immune());
	}

	Vector GetBonePos(int id) {
		matrix3x4 matrixArray[128];
		if (!this->SetupBones(matrixArray, 128, 0x100, GetTickCount64()))
			return Vector(0, 0, 0);
		else
			return Vector(matrixArray[id][0][3], matrixArray[id][1][3], matrixArray[id][2][3]);
	}

	ClientClass* GetClientClass() {
		PVOID pRenderable = (PVOID)(this + 0x8);
		typedef ClientClass*(__thiscall* oGetClientClass)(PVOID);
		return getvfunc<oGetClientClass>(pRenderable, 2)(this);
	}

	ICollideable* CollisionGroup() {
		typedef ICollideable*(__thiscall* oGetCollideable)(PVOID);
		return getvfunc<oGetCollideable>(this, 3)(this);
	}

	bool Dormant() {
		PVOID pRenderable = (PVOID)(this + 0x8);
		typedef bool(__thiscall* oGetDormant)(PVOID);
		return getvfunc<oGetDormant>(pRenderable, 9)(pRenderable);
	}

	bool IsHostage()
	{
		if (!this)
		{
			return false;
		}
		ClientClass* cClass = (ClientClass*)this->GetClientClass();

		return cClass->GetClassID() == CHostage;
	}

	bool IsChicken()
	{
		if (!this)
		{
			return false;
		}
		ClientClass* cClass = (ClientClass*)this->GetClientClass();

		return cClass->GetClassID() == CChicken;
	}

	std::array<float, 24> GetPoseParameters() {
		return *reinterpret_cast<std::array<float, 24>*>((DWORD)this + g_pOffsets->m_flPoseParameter);
	}

	float& PoseParameters() {
		return *reinterpret_cast<float*>((DWORD)this + 0xAA08);
	}

	Vector& GetEyeAngles_Editable() {
		return *reinterpret_cast<Vector*>((DWORD)this + g_pOffsets->m_angEyeAngles);
	}

	Vector* GetAbsOriginPtr() {
		return (Vector*)((DWORD)this + g_pOffsets->m_vecOrigin);
	}


	IClientCombatWeapon* GrabActive();
};

class C_BaseViewModel : public C_BaseEntity {
public:
	make_group(DWORD, GetOwner, g_pOffsets->m_hOwner);
	make_group(DWORD, GetWeapon, g_pOffsets->m_hWeapon);
};



class CMatchSystem
{
public:
	PlayerManager* GetPlayerManager()
	{
		typedef PlayerManager*(__thiscall* tKapis)(void*);
		return getvfunc<tKapis>(this, 0)(this);
	}
};

class PlayerManager
{
public:
	PlayerLocal* GetLocalPlayer(int un)
	{
		typedef PlayerLocal*(__thiscall* tTo)(void*, int);
		return getvfunc<tTo>(this, 1)(this, un);
	}
};

class PlayerLocal
{
public:
	int GetXUIDLow()
	{
		return *reinterpret_cast<int*>(reinterpret_cast<uintptr_t>(this) + 0x8);
	}

	int GetXUIDHigh()
	{
		return *reinterpret_cast<int*>(reinterpret_cast<uintptr_t>(this) + 0xC);
	}

	const char* GetName()
	{
		typedef const char*(__thiscall* tPaster)(void*);
		return getvfunc<tPaster>(this, 2)(this);
	}
};