class COffsets {
public:
	void Init(void);

	int m_iHealth;
	int m_iTeamNum;
	int m_fFlags;
	int m_nTickBase;
	int m_hActiveWeapon;
	int m_ArmorValue;
	int m_bHasHelmet;
	int m_flLowerBodyYawTarget;
	int m_vecVelocity;
	int m_bIsScoped;
	int m_aimPunchAngle;
	int m_viewPunchAngle;
	int m_lifeState;
	int m_bGunGameImmunity;
	int m_vecOrigin;
	int m_angEyeAngles;
	int m_hOwnerEntity;
	int m_nHitboxSet;
	int m_flCycle;
	int m_flSimulationTime;
	int m_nSequence;
	int m_bDormant;
	int m_Collision;
	int m_CollisionGroup;
	int m_rgflCoordinateFrame;
	int m_Model;
	int m_MoveType;
	int m_vecViewOffset;
	int m_iItemDefinitionIndex;
	int m_iClip1;
	int m_flNextPrimaryAttack;
	int m_flPostponeFireReadyTime;
	int m_iModelIndex;
	int m_hMyWearables;
	int m_flPoseParameter;
	int m_hOwner;
	int m_hWeapon;
	int deadflag;
};

extern COffsets* g_pOffsets;