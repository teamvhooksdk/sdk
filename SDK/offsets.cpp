#include "main.h"

void COffsets::Init(void)
{
	this->m_iHealth = print_netvar("DT_BasePlayer", "m_iHealth");
	this->m_iTeamNum = print_netvar("DT_BasePlayer", "m_iTeamNum");
	this->m_fFlags = print_netvar("DT_BasePlayer", "m_fFlags");
	this->m_nTickBase = print_netvar("DT_CSPlayer", "m_nTickBase");
	this->m_hActiveWeapon = print_netvar("DT_BasePlayer", "m_hActiveWeapon");
	this->m_ArmorValue = print_netvar("DT_CSPlayer", "m_ArmorValue");
	this->m_bHasHelmet = print_netvar("DT_CSPlayer", "m_bHasHelmet");
	this->m_flLowerBodyYawTarget = print_netvar("DT_CSPlayer", "m_flLowerBodyYawTarget");
	this->m_vecVelocity = print_netvar("DT_BasePlayer", "m_vecVelocity[0]");
	this->m_bIsScoped = print_netvar("DT_CSPlayer", "m_bIsScoped");
	this->m_aimPunchAngle = print_netvar("DT_BasePlayer", "m_aimPunchAngle");
	this->m_viewPunchAngle = print_netvar("DT_BasePlayer", "m_viewPunchAngle");
	this->m_lifeState = print_netvar("DT_BasePlayer", "m_lifeState");
	this->m_bGunGameImmunity = print_netvar("DT_CSPlayer", "m_bGunGameImmunity");
	this->m_vecOrigin = print_netvar("DT_BaseEntity", "m_vecOrigin");
	this->m_angEyeAngles = print_netvar("DT_CSPlayer", "m_angEyeAngles");
	this->m_hOwnerEntity = print_netvar("DT_BaseEntity", "m_hOwnerEntity");
	this->m_nHitboxSet = print_netvar("DT_CSPlayer", "m_nHitboxSet");
	this->m_flCycle = print_netvar("DT_BaseAnimating", "m_flCycle");
	this->m_flSimulationTime = print_netvar("DT_BaseEntity", "m_flSimulationTime");
	this->m_nSequence = print_netvar("DT_BaseAnimating", "m_nSequence");
	this->m_Collision = print_netvar("DT_BasePlayer", "m_Collision");
	this->m_CollisionGroup = print_netvar("DT_BasePlayer", "m_CollisionGroup");
	this->m_vecViewOffset = print_netvar("DT_BasePlayer", "m_vecViewOffset[0]");
	this->m_iItemDefinitionIndex = print_netvar("DT_BaseAttributableItem", "m_iItemDefinitionIndex");
	this->m_iClip1 = print_netvar("DT_BaseCombatWeapon", "m_iClip1");
	this->m_flNextPrimaryAttack = print_netvar("DT_BaseCombatWeapon", "m_flNextPrimaryAttack");
	this->m_flPostponeFireReadyTime = print_netvar("DT_WeaponCSBase", "m_flPostponeFireReadyTime");
	this->m_hMyWearables = print_netvar("DT_BaseCombatCharacter", "m_hMyWearables");
	this->m_flPoseParameter = print_netvar("DT_BaseAnimating", "m_flPoseParameter");
	this->m_hOwner = print_netvar("DT_BaseViewModel", "m_hOwner");
	this->m_hWeapon = print_netvar("DT_BaseViewModel", "m_hWeapon");
	this->deadflag = print_netvar("DT_CSPlayer", "deadflag");
	this->m_MoveType = m_lifeState - 3;
	this->m_rgflCoordinateFrame = m_CollisionGroup - 0x30;
	this->m_iModelIndex = 0x254;
	this->m_bDormant = 0xE9;
	this->m_Model = 0x6C;
}

COffsets* g_pOffsets = new COffsets();